#!/bin/bash

DOCKER_IMAGE=auth-2
APP_DIR=$PWD
DOCKER_IP=0.0.0.0
docker stop ${DOCKER_IMAGE}
docker rm -f ${DOCKER_IMAGE}

docker run --name ${DOCKER_IMAGE} -p ${DOCKER_IP}:3332:3332  \
  --restart=always \
  -v ${APP_DIR}:/opt/tarantool \
  -e TARANTOOL_USER_NAME=admin \
  -e TARANTOOL_USER_PASSWORD=password \
  -d tarantool/tarantool:2.10.0-rc1 tarantool /opt/tarantool/init.lua